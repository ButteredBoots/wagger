from django.apps import AppConfig


class WaggerAppConfig(AppConfig):
    name = 'wagger_app'
