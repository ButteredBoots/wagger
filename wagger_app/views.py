from django.shortcuts import render

# Create your views here.
#this function will return and render the homepage when url is called


def index_page(request):
    return render(request, 'index.html')

